//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file updateCats.h
//### @version 1.0
//###
//### Update Cats Module header- Updates the cats in the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022 
//###############################################################################

#ifndef UPDATECATS_H //Makes sure this file is only included once during program compilation
#define UPDATECATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int updateCatName(const int index, const char newName[]);
int fixCat(const int index);
int updateCatWeight(const int index, const double newWeight);
int updateCatCollar1(const cat_index index, const color newcatCollar1);
int updateCatCollar2(const cat_index index, const color newcatCollar2);
int updateLicense(const cat_index index, const license newlicense);
int updateBirthday(const cat_index index, const char birthdayString[]);

#endif 
