//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file deleteCats.c
//### @version 1.0
//###
//### Delete Cats Module - removes cats from database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022
//##############################################################################
#include "catDatabase.h"
#include <stdio.h>//For printf
#include <string.h>//For memset


extern cat_index currentCatNum; // Declared externally in catDatabase.c

int deleteAllCats(){
    memset(catdb,0,sizeof(catdb));//Resets all data in catdb to 0
    currentCatNum = 0;
    return 0;
}

int deleteCat(const cat_index index){
    if( index > currentCatNum ){
        fprintf(stderr, "%s: Cannot Delete this item at index %d as it's outside of the database.",PROGRAM_NAME, (int)index);
        return -1;
    }
    else{
        for(cat_index i = index-1; i < currentCatNum-1; i++){
            catdb[i]= catdb[i+1]; //Move the next element(i+1) one down (i) 
        }
        currentCatNum -=1;//Moves the index back one
        return 0;
    }
}
