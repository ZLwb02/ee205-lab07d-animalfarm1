//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file addCats.h
//### @version 1.0
//###
//### Add Cats Module header - adds cats to the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022
//###############################################################################

#ifndef ADDCATS_H //Makes sure this file is only included once during program compilation
#define ADDCATS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "catDatabase.h"

int addCat(const char name[], const gender gender, const breed breed, const bool isFixed, const double weight, const color collarColor1, const color collarColor2, const license license, const char birthday[]);

#endif 
