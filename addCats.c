//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file addCats.c
//### @version 1.0
//###
//### Add Cats Module - adds cats to the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022
//###############################################################################
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h> //Declares bool type
#include "catDatabase.h"

extern cat_index currentCatNum; // Declared externally in catDatabase.c as type size_t

//Used to add cat to the database
int addCat(const char name[], const gender gender, const breed breed, const bool isFixed, const double weight, const color collarColor1, const color collarColor2, const license license, const char birthdayString[]){
    birthday birthday = makeBirthday(birthdayString);//Convert birthday string to struct tm
    if( (isdbFull() == false) && (isNameValid(name) == true) && (isWeightValid(weight) == true) && (isCollarValid(collarColor1, collarColor2) == true) && (isLicenseValid(license) == true) && (isBirthdayValid(birthday) == true) ){
        strcpy(catdb[currentCatNum].name, name);
        catdb[currentCatNum].gender			= gender;
        catdb[currentCatNum].breed			= breed;
        catdb[currentCatNum].isFixed		= isFixed;
        catdb[currentCatNum].weight 		= weight;
        catdb[currentCatNum].collarColor1   =collarColor1;
        catdb[currentCatNum].collarColor2	=collarColor2;
		catdb[currentCatNum].license		=license;
        catdb[currentCatNum].birthday       =birthday;
        currentCatNum += 1;
        return currentCatNum-1;// -1 to get last cat in db, current cat num holds next cat index avalible
    }//End of if
    return -1;//Return -1 is adding cat failed
}//End of addCat

