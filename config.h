//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file config.h
//### @version 1.0
//###
//### Header file for the program
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022
//###############################################################################

#ifndef config_h //Makes sure this file is only included once during program compilation
#define config_h


#define MAX_NAME_LENGTH    50               //Maximum cat name length
#define MAX_CATS           1028             //Maximum cats allowed to be in catDatabase array
#define MAX_CAT_WEIGHT     1000             //Maximum cat weight; Minimum is hard coded to 0
#define DEBUGMODE          1                //0 for Debug mode off, 1 for on
#define EXTENSIVEDEBUG     0                 
#define PROGRAM_NAME       "Animal Farm 1"  //Used for error messages

#endif
