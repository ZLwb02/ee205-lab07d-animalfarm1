//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file updateCats.c
//### @version 1.0
//###
//### Update Cats Module- Updates the cats in the database
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022 
//###############################################################################
#include <stdbool.h> //For bool data type
#include <stdio.h> //For printing
#include <stdlib.h> 
#include <string.h> //For strcpy
#include "catDatabase.h"
#include "config.h"

extern cat_index currentCatNum; // Declared externally in catDatabase.c

int updateCatName(const int index, const char newName[]){
    if(isNameValid(newName) == true){
        strcpy(catdb[index].name, newName);
        return 0;
    }
    return -1;
}//End of upDateCatName

int fixCat(const cat_index index){
    if( isFixingCatPossible(index) == false){
        fprintf(stderr,"%s: %s has already been fixed... Thats not good... ", PROGRAM_NAME, catdb[index].name);
		return -1;
    }
    catdb[index].isFixed = true;
    return 0;
}//End of fixCat

int updateCatWeight(const cat_index index, const double newWeight){
	if(isWeightValid(newWeight) == true){
		catdb[index].weight = newWeight;
		return 0;
    }
    return -1;
}//End of updateCatWeight

int updateCatCollar1(const cat_index index, const color newCatCollar1){
    if( isCollarValid(newCatCollar1, catdb[index].collarColor2) == true){
        catdb[index].collarColor1 = newCatCollar1;
        return 0;
    }
    return -1;
}//End of updateCatCollar1

int updateCatCollar2(const cat_index index, const color newCatCollar2){
    if( isCollarValid(catdb[index].collarColor1, newCatCollar2) == true ){
        catdb[index].collarColor2 = newCatCollar2;
        return 0;
    }
    return -1;
}//End of updateCatCollar2

int updateLicense(const cat_index index, const license newLicense){
    if (isLicenseValid(newLicense) == true){
        catdb[index].license = newLicense;
        return 0;
    }
    return -1;
}//End of updateLicense

int updateBirthday(const cat_index index, const char birthdayString[]){
    birthday birthday = makeBirthday(birthdayString);//Convert birthday string to struct tm
    if(isBirthdayValid(birthday) == true){
        catdb[index].birthday = birthday;
        return 0;
    }
    return -1;
}






