//##############################################################################
//###         University of Hawaii, College of Engineering
//### @brief  Lab 07d -AnimalFarm1 - EE 205 - Spr 2022
//###
//### @file catDatabase.h
//### @version 1.0
//###
//### Cat Database header - Defines all of the enums,the arrays, max array & num sizes, 
//###
//### @author Zack Lown <zlown@hawaii.edu>
//### @date   05 Mar 2022
//##############################################################################

#ifndef catDatabase_h //Makes sure this file is only included once during program compilation
#define catDatabase_h

#include <stdbool.h> //Defines bool data type for isFixed in struct Cat
#include <stddef.h>  // Allows for size_t. Could also use stdio.h if needed later
#include <time.h>
#include "config.h" // For MAX's'

//Defines the enum's
typedef enum {UNKNOWN_GENDER,MALE,FEMALE} gender;
typedef enum {UNKNOWN_BREED,MAINE_COON,MANX,SHORTHAIR,PERSIAN,SPHYNX} breed;
typedef enum {BLACK,WHITE,PURPLE,BLUE,AQUA,GREEN,YELLOW,ORANGE,RED,PINK,BROWN,RAINBOW,OTHER} color;


//Defines aliases
typedef size_t cat_index; // Sets a new datatype , cat_index, as alias to size_t
typedef unsigned long long license;
typedef struct tm birthday;

//This creates a new datatype (cats) with the applicable fields.
struct Cat {
  //Data type           Data name
    char                name[MAX_NAME_LENGTH];
    //struct tm			tm;			
    gender              gender;         //enum genders declared above
    breed               breed;          //enum breeds declared above
    bool                isFixed;
    float               weight;
    color               collarColor1;   //enum color declared above
    color               collarColor2;   //...
    unsigned long long  license;
    birthday            birthday;       //Used for storing birthday
};


extern int initializeDatabase();
extern struct Cat catdb[MAX_CATS]; //This doesnt allocate any memory, just says this fn is declared in catDatabase.c

//Function Declarations
bool isdbFull();
bool isNameValid(const char name[]);
bool isWeightValid(double weight);
bool isLicenseValid(license license);
bool isCollarValid(color collarColor1, color collarColor2);
bool isFixingCatPossible(cat_index index);
birthday makeBirthday(const char birthday[]);
bool isBirthdayValid(birthday birthday);
#endif

